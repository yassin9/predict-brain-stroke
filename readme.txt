يحتوي على افضل النماذج حسب الملفات التالية  (machineLearningProject_imbalancedData_solution_WithOnlyBestModel) (الملف الاساسي)

يحتوي الملف على تصور للبيانات و ماذا تحتوي البيانات من قيم مستمرة و متقطعة (visualize_data)

يحتوي الملف على تجربة للبيانات كما هي من دون معالجة على النماذج المراد تجربتها (machineLearningProjectbaseline)

يحتوي الملف على تجربة معالجة كافة البيانات من ازالة قيم فارغة و ازاله القيم الاسمية و تحويل الى مجال ثابت الخ مع اختيار المميزات المناسبة مع اعادة تصور للبيانات اسم الملف (machineLearningProjectWithNormalizeData)

يحتوي الملف على تجربة كل ما سبق من كل طرائق لاختيار المميزات وكل طرق اعادة تصور البيانات مع حلول لتوازن البيانات اسم الملف (machinrLearnigProject_Imbalanced_Solution_With_all_Experiments)

يحتوي على ما يحتويه الملف السابق ولكن يحتوي فقط على الافضل (machineLearningProject_imbalancedData_solution_WithOnlyBestModel)

رابط مجموعة البيانات: (https://www.kaggle.com/datasets/fedesoriano/stroke-prediction-dataset)






